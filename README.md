# [Math4CNS: 計算論的神経科学を学ぶための数学](https://math4cns.yendo.info)

[![Netlify Status](https://api.netlify.com/api/v1/badges/0538be46-896b-4453-9cdd-38709232d4e7/deploy-status)](https://app.netlify.com/sites/math4cns/deploys)

[Math4CNS: 計算論的神経科学を学ぶための数学](https://math4cns.yendo.info) のソースコードです。
このサイトは MkDocs を用いて作成され、Netlify でホスティングされています。

誤記の指摘や内容のリクエストは、Issue or MR にてお願いいたします。
